<?php

namespace BoneyBone\BillingService;

use Illuminate\Support\ServiceProvider;
use BoneyBone\BillingService\CacheStore;
use BoneyBone\BillingService\Contracts\Store;
use BoneyBone\BillingService\Contracts\RequestClient;
use BoneyBone\BillingService\Registries\EntityRegistry;
use BoneyBone\BillingService\Registries\RequestClientRegistry;
use BoneyBone\BillingService\Exceptions\BillingServiceException;
use BoneyBone\BillingService\Request\GuzzleClient;
use BoneyBone\BillingService\Entities\{
    CreditNoteEntity,
    CustomerEntity,
    DebitNoteEntity,
    InvoiceEntity,
    ReceiptEntity
};

class BillingServiceProvider extends ServiceProvider
{

    /**
     * The entities that should be registered.
     *
     * @var  array $entities
     */
    protected $entities = [
        'invoice' => InvoiceEntity::class,
        'customer' => CustomerEntity::class,
        'receipt' => ReceiptEntity::class,
        'credit_note' => CreditNoteEntity::class,
        'creditnote' => CreditNoteEntity::class,
        'credit-note' => CreditNoteEntity::class,
        'credit note' => CreditNoteEntity::class,
        'debit_note' => DebitNoteEntity::class,
        'debitnote' => DebitNoteEntity::class,
        'debit-note' => DebitNoteEntity::class,
        'debit note' => DebitNoteEntity::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            Store::class,
            CacheStore::class
        );

        $this->app->singleton(RequestClientRegistry::class);
        $this->app->singleton(EntityRegistry::class);

        $this->app->bind('bb-billing-service-sdk', BillingService::class);
        $this->app->singleton(BillingService::class);

        $this->registerRequestClients();

        $this->registerEntities();

        $this->bindDefaultClient();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Publish The Configurations.
        $this->publishes([
            __DIR__ . '/config/billing-service.php' => config_path('billing-service.php')
        ], 'billing-service');
    }

    /**
     * Register the RequestClient.
     *
     * @return void
     */
    protected function registerRequestClients() {
        $this->app->make('bb-billing-service-sdk')->requestClient('guzzle', GuzzleClient::class);
    }

    /**
     * Bind the default client to the contract.
     *
     * @return void
     */
    public function bindDefaultClient() {

        if( ! file_exists(config_path('billing-service.php')) ) return;

        if(
            ! $this->app->make(RequestClientRegistry::class)->has(config('billing-service.client')) &&
            ! is_null(config('billing-service.client'))
        ) {
            throw new BillingServiceException(
                sprintf("Billing Service Client [%s] does not exists.",
                    config('billing-service.client')
                )
            );
        }

        $this->app->bind(
            RequestClient::class,
            get_class($this->app->make(RequestClientRegistry::class)->get(config('billing-service.client')))
        );
    }

    /**
     * Register all Entities.
     *
     * @return void
     */
    protected function registerEntities() {
        foreach($this->entities as $alias => $entity) {
            $this->app->make(EntityRegistry::class)->register(
                $alias, $this->app->make($entity)
            );

            $this->app->make(EntityRegistry::class)->register(
                $entity, $this->app->make($entity)
            );
        }
    }
}
