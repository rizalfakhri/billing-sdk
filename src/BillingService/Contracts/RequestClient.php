<?php

namespace BoneyBone\BillingService\Contracts;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface RequestClient {

    /**
     * Send HTTP Request.
     *
     * @param  RequestInterface   $request
     * @param  array              $options
     * @return ResponseInterface
     */
    public function sendRequest(RequestInterface $request, array $options = []) : ResponseInterface;

}

