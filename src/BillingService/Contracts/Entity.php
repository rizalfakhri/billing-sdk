<?php

namespace BoneyBone\BillingService\Contracts;

interface Entity {

    /**
     * Get the entity listings.
     *
     * @param  array $options
     * @return array
     */
    public function list(array $options = []);

    /**
     * Get the individual entity.
     *
     * @param  string|integer $id
     * @param  array          $options
     * @return array
     */
    public function get($id, array $options = []);

    /**
     * Create new entity.
     *
     * @param  array $data
     * @param  array $options
     * @return array
     */
    public function create(array $data, array $options = []);

    /**
     * Update entity.
     *
     * @param  string|int  $id
     * @param  array       $options
     * @return array
     */
    public function update($id, array $options = []);

    /**
     * Delete entity.
     *
     * @param  string|int  $id
     * @param  array       $options
     * @return array
     */
    public function delete($id, array $options = []);
}
