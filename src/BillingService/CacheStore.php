<?php

namespace BoneyBone\BillingService;

use Cache;
use BoneyBone\BillingService\Contracts\Store;

class CacheStore implements Store {

    /**
     * The session prefix.
     *
     * @var  string
     */
    protected $prefix = 'bb_billing_service_';

    /**
     * Set item to the store.
     *
     * @param  string $key
     * @param  mixed  $value
     * @return void
     */
    public function set($key, $value) {
        return Cache::put($this->formatKey($key), $value, 60 * 60 * 60);
    }

    /**
     * Get the item from store using its key.
     *
     * @param  string $key
     * @param  mixed  $default  The dfault value to return if no data found in the store.
     * @return mixed
     */
    public function get($key, $default = null) {
        $key = $this->formatKey($key);

        return Cache::get($key, $default);
    }

    /**
     * Delete item from store using its key.
     *
     * @param  string $key
     * @return void
     */
    public function delete($key) {
        $key = $this->formatKey($key);

        Cache::forget($key);
    }

    /**
     * Combine the original key with the prefix.
     *
     * @param  string $key
     * @return string
     */
    public function formatKey($key) {
        return sprintf("%s%s", $this->prefix, $key);
    }
}
