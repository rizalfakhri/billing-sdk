<?php

namespace BoneyBone\BillingService;

use GuzzleHttp\Psr7;
use Illuminate\Support\Facades\Artisan;
use BoneyBone\BillingService\Contracts\Entity;
use BoneyBone\BillingService\Contracts\Store;
use BoneyBone\BillingService\Contracts\RequestClient;
use BoneyBone\BillingService\Registries\EntityRegistry;
use BoneyBone\BillingService\Registries\RequestClientRegistry;
use BoneyBone\BillingService\Exceptions\BillingServiceException;
use BoneyBone\BillingService\Exceptions\BillingEntityNotFoundException;
use Illuminate\Contracts\Foundation\Application;

class BillingService {

    /**
     * The Store Instance.
     *
     * @var  Store $store
     */
    public $store;

    /**
     * The RequestClientRegistry instance.
     *
     * @var  RequestClientRegistry $requestClientRegistry
     */
    protected $requestClientRegistry;

    /**
     * The EntityRegistry instance.
     *
     * @var  EntityRegistry $entityRegistry
     */
    protected $entityRegistry;

    /**
     * The Laravel Application instance.
     *
     * @var  Application $app
     */
     protected $app;

    /**
     * Build the Billing Service instance.
     *
     * @param  Store $store
     */
    public function __construct(
        Store $store,
        RequestClientRegistry $requestClientRegistry,
        EntityRegistry $entityRegistry,
        Application $app
    )
    {
        $this->store = $store;
        $this->requestClientRegistry = $requestClientRegistry;
        $this->entityRegistry = $entityRegistry;
        $this->app = $app;

        if( file_exists( config_path('billing-service.php') ) ) {
            $this->validateSDK();
        }
    }

    /**
     * Use the Entity.
     *
     * @param  string $entity
     * @return Entity
     *
     * @throws BillingEntityNotFoundException When the entity does not exists on the registry.
     */
    public function use($entity) {
        if( $this->entityRegistry->has($entity) ) {
            return $this->entityRegistry->get($entity);
        }

        throw new BillingEntityNotFoundException($entity);
    }

    /**
     * Check if the SDK is authenticated.
     *
     * @return bool
     * @throws Exception When the error occured.
     */
    public function isAuthenticated() {
        return (bool) $this->store->get('access_token');
    }

    /**
     * Authenticate the SDK.
     *
     * @return void
     */
    public function authenticateSDK() {
        $request = new Psr7\Request('POST', 'authenticate');

        /**
         * The Response is instanceof ResponseInterface
         *
         * @see https://www.php-fig.org/psr/psr-7/#33-psrhttpmessageresponseinterface
         */
        $response = app(RequestClient::class)->sendRequest($request, [
            'form_params' => [
                'client_id'     => config('billing-service.client_id'),
                'client_secret' => config('billing-service.client_secret')
            ]
        ]);

        if( $response->getStatusCode() == 200 ) {
            $data = json_decode($response->getBody()->getContents(), true);

            if( isset($data['access_token']) && !is_null($data['access_token']) ) {
                $this->setAccessToken($data['access_token']);
            }
        }

    }

    /**
     * Register new Request Client.
     *
     * @param  string               $key
     * @param  string|RequestClient $class
     * @return void
     */
    public function requestClient($key, $class) {

        if( is_string($class) ) {
            $class = $this->app->make($class);
        }

        if( $class instanceof RequestClient )
            return $this->requestClientRegistry->register($key, $class);

        throw new BillingServiceException("The class should be instance of RequestClient");
    }

    /**
     * Register new Entity.
     *
     * @param  string        $alias
     * @param  string|Entity $entity
     * @return void
     */
    public function entity($alias, $entity) {

        if( is_string($entity) ) {
            $entity = $this->app->make($entity);
        }

        if( $entity instanceof Entity ) {
            $this->entityRegistry->register($alias, $entity);
            $this->entityRegistry->register(get_class($entity), $entity);

            return;
        }

        throw new BillingServiceException("The entity should be instance of Entity.");
    }

    /**
     * Validate the SDK requirements.
     *
     * @return void
     * @throws BillingServieException
     */
    public function validateSDK() {
        if(
            ! is_null(config('billing-service.client')) ||
            ! is_null(config('billing-service.client_id')) ||
            ! is_null(config('billing-service.client_secret')) ||
            ! is_null(config('billing-service.endpoint'))
        )
        {
            return;
        }

        if(
            ! $this->requestClientRegistry->has(config('billing-service.client')) &&
            ! is_null(config('billing-service.client'))
        ) {
            throw new BillingServiceException(
                sprintf("Billing Service Client [%s] does not exists.",
                    config('billing-service.client')
                )
            );
        }

        throw new BillingServiceException(
            "Please run 'php artisan vendor:publish --tag=billing-service' command " .
            "and configure the .env file with correct Billing Credentials."
        );
    }

    /**
     * Get the billing service endpoint.
     *
     * @return string|null
     */
    public function getBillingServiceEndpoint() {
        $endpoint  = config('billing-service.endpoint');

        return sprintf("%s/api/external/", $endpoint);
    }

    /**
     * Get billing service access token.
     *
     * @return string|null
     */
    public function getAccessToken() {
        return $this->store->get('access_token');
    }

    /**
     * Set billing service access token.
     *
     * @param  string $accessToken
     * @return void
     */
    public function setAccessToken($accessToken) {
        return $this->store->set('access_token', $accessToken);
    }
}
