<?php

namespace BoneyBone\BillingService\Request;

use BillingService;
use GuzzleHttp\Client;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use BoneyBone\BillingService\Contracts\RequestClient;

class GuzzleClient implements RequestClient {

    /**
     * The GuzzleClient.
     *
     * @var  Client $client
     */
    protected $client;

    /**
     * Build the Client.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => BillingService::getBillingServiceEndpoint()
        ]);
    }

    /**
     * Send HTTP Request.
     *
     * @param  RequestInterface   $request
     * @param  array              $options
     * @return ResponseInterface
     */
    public function sendRequest(RequestInterface $request, array $options = []) : ResponseInterface {
        return $this->client->send($request, $options);
    }

}
