<?php

namespace BoneyBone\BillingService\Entities;

use BoneyBone\BillingService\WithRequests;
use BoneyBone\BillingService\Contracts\Entity;

final class ReceiptEntity implements Entity {

    use WithRequests {
        WithRequests::get as getRequest;
    }

    /**
     * Get the entity listings.
     *
     * @param  array $options
     * @return array
     */
    public function list(array $options = []) {

        $res = $this->post('receipts/list', [
            'form_params' => $options
        ]);

        return json_decode(
            $res->getBody()->getContents(), true
        );
    }

    /**
     * Get the individual entity.
     *
     * @param  string|integer $id
     * @param  array          $options
     * @return array
     */
    public function get($id, array $options = []) {

        if( isset($options['id']) ) {
            $options['id'] = array_merge([$id], $options['id']);
        }
        else
        {
            $options['id'] = [$id];
        }

        $options['id'] = implode(",", $options['ids']);

        $res = $this->post('receipts/get', [
            'form_params' => $options
        ]);

        return json_decode(
            $res->getBody()->getContents(), true
        );
    }

    /**
     * Create new entity.
     *
     * @param  array $data
     * @param  array $options
     * @return array
     */
    public function create(array $data, array $options = []) {
        $res = $this->post('receipts/create', [
            'form_params' => $data
        ]);

        return $res->getBody()->getContents();
    }

    /**
     * Update entity.
     *
     * @param  string|int  $id
     * @param  array       $options
     * @return array
     */
    public function update($id, array $options = []) {

        if( isset($options['receipt']) ) {
            $options['receipt']['id'] = $id;
        }
        else
        {
            $options['receipt']['id'] = $id;
        }

        $res = $this->post('receipts/update', [
            'form_params' => $options
        ]);

        return $res->getBody()->getContents();
    }

    /**
     * Delete entity.
     *
     * @param  string|int  $id
     * @param  array       $options
     * @return array
     */
    public function delete($id, array $options = []) {
        $options['id'] = $id;

        $res = $this->post('receipts/delete', [
            'form_params' => $options
        ]);

        return $res->getBody()->getContents();
    }
}
