<?php

namespace BoneyBone\BillingService\Entities;

use BoneyBone\BillingService\WithRequests;
use BoneyBone\BillingService\Contracts\Entity;

final class DebitNoteEntity implements Entity {

    use WithRequests {
        WithRequests::get as getRequest;
    }

    /**
     * Get the entity listings.
     *
     * @param  array $options
     * @return array
     */
    public function list(array $options = []) {

        $res = $this->post('debit_note/list', [
            'json' => $options
        ]);

        return json_decode(
            $res->getBody()->getContents(), true
        );
    }

    /**
     * Get the individual entity.
     *
     * @param  string|integer $id
     * @param  array          $options
     * @return array
     */
    public function get($id, array $options = []) {

        if( isset($options['id']) ) {
            $options['id'] = array_merge([$id], $options['id']);
        }
        else
        {
            $options['id'] = [$id];
        }

        $options['id'] = implode(",", $options['ids']);

        $res = $this->post('debit_note/get', [
            'json' => $options
        ]);

        return json_decode(
            $res->getBody()->getContents(), true
        );
    }

    /**
     * Create new entity.
     *
     * @param  array $data
     * @param  array $options
     * @return array
     */
    public function create(array $data, array $options = []) {
        $res = $this->post('debit_note/create', [
            'form_params' => $data
        ]);

        return $res->getBody()->getContents();
    }

    /**
     * Update entity.
     *
     * @param  string|int  $id
     * @param  array       $options
     * @return array
     */
    public function update($id, array $options = []) {

        if( isset($options['debit_note']) ) {
            $options['debit_note']['id'] = $id;
        }
        else
        {
            $options['debit_note']['id'] = $id;
        }

        $res = $this->post('debit_note/update', [
            'json' => $options
        ]);

        return $res->getBody()->getContents();
    }

    /**
     * Delete entity.
     *
     * @param  string|int  $id
     * @param  array       $options
     * @return array
     */
    public function delete($id, array $options = []) {
        $options['id'] = $id;

        $res = $this->post('debit_note/delete', [
            'json' => $options
        ]);

        return $res->getBody()->getContents();
    }
}
