<?php

namespace BoneyBone\BillingService\Entities;

use BoneyBone\BillingService\Contracts\Entity;

abstract class BaseEntity implements Entity {

    /**
     * Get the entity listings.
     *
     * @param  array $options
     * @return array
     */
    abstract public function list(array $options = []);

    /**
     * Get the individual entity.
     *
     * @param  string|integer $id
     * @param  array          $options
     * @return array
     */
    abstract public function get($id, array $options = []);

    /**
     * Create new entity.
     *
     * @param  array $data
     * @param  array $options
     * @return array
     */
    abstract public function create(array $data, array $options = []);

    /**
     * Update entity.
     *
     * @param  string|int  $id
     * @param  array       $options
     * @return array
     */
    abstract public function update($id, array $options = []);

    /**
     * Delete entity.
     *
     * @param  string|int  $id
     * @param  array       $options
     * @return array
     */
    abstract public function delete($id, array $options = []);
}
