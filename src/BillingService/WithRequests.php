<?php

namespace BoneyBone\BillingService;

use BillingService;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;
use BoneyBone\BillingService\Contracts\RequestClient;

trait WithRequests {

    /**
     * Determine if the request require authenticated.
     *
     * @var  bool $useAuth
     */
    protected $useAuth = true;

    /**
     * Do not Use the authenticcation.
     * This will detach the Bearer token from the HTTP Request headers.
     *
     * @return self
     */
    public function dontUseAuth() {
        $this->useAuth = false;
    }

    /**
     * Handle the request.
     *
     * @param  string $method
     * @param  string $uri
     * @param  array  $options
     * @return ResponseInterface
     */
    public function request($method, $uri, array $options = []) : ResponseInterface {
        $request = new Request($method, $uri);

        $requestClient = app(RequestClient::class);

        if( $this->useAuth ) {

            if( ! isset($options['headers']) ) {
                $options['headers'] = [
                    'Authorization' => sprintf("Bearer %s", BillingService::getAccessToken())
                ];
            }
            else
            {
                $options['headers'] = array_merge(
                    $options['headers'],
                    [
                        'Authorization' => sprintf("Bearer %s", BillingService::getAccessToken())
                    ]
                );
            }

        }

        $options['headers'] = array_merge(
            $options['headers'],
            config('billing-service.headers') ?? []
        );

        return $requestClient->sendRequest(
            $request, $options
        );
    }

    /**
     * Create and send an HTTP GET request.
     *
     * Use an absolute path to override the base path of the client, or a
     * relative path to append to the base path of the client. The URL can
     * contain the query string as well.
     *
     * @param string|UriInterface $uri     URI object or string.
     * @param array               $options Request options to apply.
     *
     * @throws GuzzleException
     */
    public function get($uri, array $options = []): ResponseInterface
    {
        return $this->request('GET', $uri, $options);
    }

    /**
     * Create and send an HTTP HEAD request.
     *
     * Use an absolute path to override the base path of the client, or a
     * relative path to append to the base path of the client. The URL can
     * contain the query string as well.
     *
     * @param string|UriInterface $uri     URI object or string.
     * @param array               $options Request options to apply.
     *
     * @throws GuzzleException
     */
    public function head($uri, array $options = []): ResponseInterface
    {
        return $this->request('HEAD', $uri, $options);
    }

    /**
     * Create and send an HTTP PUT request.
     *
     * Use an absolute path to override the base path of the client, or a
     * relative path to append to the base path of the client. The URL can
     * contain the query string as well.
     *
     * @param string|UriInterface $uri     URI object or string.
     * @param array               $options Request options to apply.
     *
     * @throws GuzzleException
     */
    public function put($uri, array $options = []): ResponseInterface
    {
        return $this->request('PUT', $uri, $options);
    }

    /**
     * Create and send an HTTP POST request.
     *
     * Use an absolute path to override the base path of the client, or a
     * relative path to append to the base path of the client. The URL can
     * contain the query string as well.
     *
     * @param string|UriInterface $uri     URI object or string.
     * @param array               $options Request options to apply.
     *
     * @throws GuzzleException
     */
    public function post($uri, array $options = []): ResponseInterface
    {
        return $this->request('POST', $uri, $options);
    }

    /**
     * Create and send an HTTP PATCH request.
     *
     * Use an absolute path to override the base path of the client, or a
     * relative path to append to the base path of the client. The URL can
     * contain the query string as well.
     *
     * @param string|UriInterface $uri     URI object or string.
     * @param array               $options Request options to apply.
     *
     * @throws GuzzleException
     */
    public function patch($uri, array $options = []): ResponseInterface
    {
        return $this->request('PATCH', $uri, $options);
    }

    /**
     * Create and send an HTTP DELETE request.
     *
     * Use an absolute path to override the base path of the client, or a
     * relative path to append to the base path of the client. The URL can
     * contain the query string as well.
     *
     * @param string|UriInterface $uri     URI object or string.
     * @param array               $options Request options to apply.
     *
     * @throws GuzzleException
     */
    public function delete($uri, array $options = []): ResponseInterface
    {
        return $this->request('DELETE', $uri, $options);
    }

}
