<?php

namespace BoneyBone\BillingService\Facades;

use Illuminate\Support\Facades\Facade;

class BillingService extends Facade {

    /**
     * Return the container binding string.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
        return 'bb-billing-service-sdk';
    }

}



