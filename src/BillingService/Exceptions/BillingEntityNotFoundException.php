<?php

namespace BoneyBone\BillingService\Exceptions;

class BillingEntityNotFoundException extends BillingServiceException {

    /**
     * Build the Exception instance.
     *
     * @return Exception
     */
    public function __construct($entity)
    {
        return parent::__construct(
            sprintf("Billing Entity [%s] does not exists.", $entity)
        );
    }

}
