<?php

namespace BoneyBone\BillingService\Exceptions;

use Exception;

class BillingServiceException extends Exception {

    /**
     * Build the exception instance.
     *
     * @param  string     $message
     * @param  string|int $code
     */
    public function __construct($message = null, $code = 500)
    {
        return parent::__construct($message, $code);
    }
}
