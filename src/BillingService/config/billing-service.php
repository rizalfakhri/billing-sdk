<?php

/**
 * This configuration file is to configure the Billing Service.
 *
 * @return array
 */
return [

    /**
     * The HTTP Client.
     *
     * Since the SDK is fully comply with PSR-7 & PSR-18, you can change the HTTP Client to whatever you want.
     * Just create the binding bridge between the SDK & the Client. And you ready to go.
     *
     * Available Clients: guzzle
     */
    'client' => 'guzzle',

    /**
     * Add Additional Headers to the Request.
     */
    'headers' => [],

    /**
     * The Billing Service Base Configuration.
     */
    'client_id'       => env('BILLING_SERVICE_CLIENT_ID'),
    'client_secret'   => env('BILLING_SERVICE_CLIENT_SECRET'),
    'endpoint'        => env('BILLING_SERVICE_ENDPOINT')
];
