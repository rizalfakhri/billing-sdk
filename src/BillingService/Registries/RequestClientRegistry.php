<?php

namespace BoneyBone\BillingService\Registries;

use BoneyBone\BillingService\Contracts\RequestClient;

class RequestClientRegistry {

    /**
     * Registered Clients.
     *
     * @var  array $clients
     */
    protected $clients = [];

    /**
     * Register new RequestClient.
     *
     * @param  string         $key
     * @param  RequestClient  $client
     * @param  bool           $override
     */
    public function register($key, RequestClient $client, $override = false) {
        if( $this->has($key) ) {
            $this->clients[$key] = ($override) ? $client : $this->clients[$key];
        }
        else
        {
            $this->clients[$key] = $client;
        }
    }

    /**
     * Determine if the client with the given key exists.
     *
     * @param  string $key
     * @return bool
     */
    public function has($key) {
        return (bool) isset($this->clients[$key]);
    }

    /**
     * Get the client by its key.
     *
     * @param  string $key
     * @return RequestClient|null
     */
    public function get($key) {
        if($this->has($key)) return $this->clients[$key];
    }

    /**
     * Get all registered Client.
     *
     * @return array
     */
    public function all() {
        return $this->clients;
    }
}
