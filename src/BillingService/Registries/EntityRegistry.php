<?php

namespace BoneyBone\BillingService\Registries;

use BoneyBone\BillingService\Contracts\Entity;

class EntityRegistry {

    /**
     * Registered Entities.
     *
     * @var  array $entities
     */
    protected $entities = [];

    /**
     * Register new Entity.
     *
     * @param  string $key
     * @param  Entity $entity
     * @param  bool   $override
     * @return void
     */
    public function register($key, Entity $entity, $override = false) {
        if( $this->has($key) )  {
            $this->entities[$key] = ($override) ? $entity : $this->entities[$key];
        }
        else
        {
            $this->entities[$key] = $entity;
        }
    }

    /**
     * Determine if the entity with the given key exists.
     *
     * @param  string $key
     * @return bool
     */
    public function has($key) {
        return (bool) isset($this->entities[$key]);
    }

    /**
     * Get the entity with its key.
     *
     * @param  string $key
     * @return Entity|null
     */
    public function get($key) {
        if( $this->has($key) ) return $this->entities[$key];
    }

    /**
     * Get all registered entities.
     *
     * @return array
     */
    public function all() {
        return $this->entities;
    }
}
