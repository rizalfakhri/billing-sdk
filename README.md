# BILLING SERVICE SDK

This is the Billing SDK for Laravel Application using the [IX Billing Engine](https://gitlab.com/boneyboneventures/internal/invoice-api).

To use this SDK, you need the `Client ID` & `Client Secret` from the IX Billing.

-----

# Installation

Install the package using Compser

```bash
composer require boneybone/billing-sdk
```

# Configuration

Publish the configuration file using the `vendor:publish` artisan command.

```bash
php artisan vendor:publish --tag=billing-service
```

Set the `.env` file using the following variables

```
BILLING_SERVICE_CLIENT_ID=
BILLING_SERVICE_CLIENT_SECRET=
BILLING_SERVICE_ENDPOINT=
```

If you are intended to use `guzzlehttp/guzzle` as your HTTP Client, set the `config/billing-service.php` client to `guzzle`.

```php
// config/billing-service.php

'client' => 'guzzle'

...
```

Then install the Guzzle.

```
composer require guzzlehttp/guzzle
```

# How to Use

### Entities

Every endpoint on the billing service is an `Entity` on this SDK, so if you want to use the `/invoices` endpoint, you need to use the `BoneyBone\BillingService\Entities\InvoiceEntity` Entity.
But each entity has its own aliases, see below.

| Entity | Alias | Class | How To Use |
|--------|-------|-------|------------|
| Invoices | `invoice` | `BoneyBone\BillingService\Entities\InvoiceEntity` | `BillingService::use('invoice')` or `BillingService::use(InvoiceEntity::class)` |

For now only 1 entity is available, will update accordingly.

The `Entity` is consists of `5` methods, `list()`, `get()`, `create()`, `update()` & `delete()` as per `Entity Interface` at `BoneyBoney\BillingService\Contracts\Entity`.

Each of the `Entity` methods accepts an `array` payload on the last parameters, so you can pass any payload to the Billing Server.

```php
BillingService::use('invoice')->list([
    'id' => 'this is invoice id'
]);

BillingService::use('invoice')->list([
    'args' => [
        'customer_id' => 'this is customer id'
    ]
]);

BillingService::use('invoice')->get($id);

BillingService::use('invoice')->delete($id);

BillingService::use('invoice')->update($id, [
    // array payload
]);
```

### The Billing Facade

The `BillingService` facade is like the gate between your code and the SDK, using this facade you can register  `Entity`, `RequestClient`, Get the Access Token, Endpoint, etc.


This SDK is [`PSR-7`](https://www.php-fig.org/psr/psr-7/)  compatible, so you are free to use any of the http client you want to use.

The default client is `guzzle` but you are free to change & extend the Client, here's how.

#### Extending RequestClient

The `RequestClient` is the bridge between the SDK and the Internet, the client job is to send the HTTP request to the Billing Server, and it only requires `1` method.

And it needs to implements the `BoneyBone\BillingService\Contracts\RequestClient`.

```php
<?php

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use BoneyBone\BillingService\Contracts\RequestClient

class YourClient implements RequestClient {

    /**
     * Send HTTP Request.
     *
     * @param  RequestInterface   $request
     * @param  array              $options
     * @return ResponseInterface
     */
    public function sendRequest(RequestInterface $request, array $options = []) : ResponseInterface {
        // here's how you implement tht HTTP request to the server.
        // the $options format is follow the Guzzle Request Options, so each of guzzle options valid here
        // and you need to parse that options
        // @see http://docs.guzzlephp.org/en/stable/request-options.html
    }

}
```

As you can see the `sendRequest()` method accepts the PSR's `RequestInterface` as a parameter and expect the PSR's `ResponseInterface` as return value.

And then you can register your `RequestClient` using the facade.
```php
// app/Providers/AppServiceProvider.php

BillingService::requestClient('your-request-client-name', YourClient::class);
```

And finally you can set your client as default client at `config/billing-service.php`

```php
...
'client' => 'your-request-client-name'
...
```

Done! The SDK now will use your client as the HTTP Client.

#### Create new Entity

Since not all of the Billing Endpoint is covered by the SDK, you might need to implement & register it yourself. Here's how.

As already mentioned above, the `Entity` is consists of 5 methods, `list()`, `get()`, `create()`, `update()`, `delete()`, since the Entity is implementing the `BoneyBone\BillingService\Contracts\Entity`.

This is how it looks

```php
<?php

namespace YourNamespace\Entities;

use BoneyBone\BillingService\WithRequests;
use BoneyBone\BillingService\Contracts\Entity;

final class CreditNoteEntity implements Entity {

    use WithRequests {
        WithRequests::get as getRequest;
    }

    /**
     * Get the entity listings.
     *
     * @param  array $options
     * @return array
     */
    public function list(array $options = []) {
        $res = $this->get('/credit_not_endpoints');

        return $res->getBody()->getContents();
    }

    /**
     * Get the individual entity.
     *
     * @param  string|integer $id
     * @param  array          $options
     * @return array
     */
    public function get($id, array $options = []) {
        //
    }

    /**
     * Create new entity.
     *
     * @param  array $data
     * @param  array $options
     * @return array
     */
    public function create(array $data, array $options = []) {
        //
    }

    /**
     * Update entity.
     *
     * @param  string|int  $id
     * @param  array       $options
     * @return array
     */
    public function update($id, array $options = []) {
        //
    }

    /**
     * Delete entity.
     *
     * @param  string|int  $id
     * @param  array       $options
     * @return array
     */
    public function delete($id, array $options = []) {
        //
    }
}
```

The Entity's job is to providing the data from the API Server to the client, and should return an array object containing data from server.

To comply with `PSR-7` this SDK is come with the `BoneyBone\BillingService\WithRequests` Trait, to provide you basic of HTTP request methods, the `WithRequests` Trait contains all of HTTP method you need to do HTTP Request to server.

Such as `get()`, `post()`, `patch()`, etc. But since the `Entity` is reserve the `get()` method, the trait is overrided. You need to alias the `get()` method using the following code while importing the trait.

```php
use WithRequests {
    WithRequests::get as getRequest;
}
```

And finally you need to register the Entity to the SDK.

```php
BillingService::entity('credit-note', CreditNoteEntity::class);
```

And Done!
